﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = 4;

            {
                Console.Clear();

            bum:
                Console.WriteLine($"          MENU- please select a number           ");
                Console.WriteLine($"                                                 ");
                Console.WriteLine($"    01. How many days have you been here for??   ");
                Console.WriteLine($"    02. Calculate Grade Aveage                   ");
                Console.WriteLine($"    03.                                          ");
                Console.WriteLine($"    04. List five Favorte foods                  ");
                Console.WriteLine($"Please select an option");
                menu = int.Parse(Console.ReadLine());

                if (menu == 1)
                {
                    Console.Clear();

                    Console.WriteLine("please enter your date of birth (MM/DD/YYYY)");
                    string birthDateString = Console.ReadLine();
                    DateTime birthDate;
                    if (DateTime.TryParse(birthDateString, out birthDate))
                    {
                        DateTime today = DateTime.Now;
                        Console.WriteLine("You are {0} days old", (today - birthDate).Days);
                        Console.WriteLine("press any key to go to main menu");
                        Console.ReadKey();
                        goto bum;
                    }
                }
                else Console.WriteLine("Incorrect date format!");
            } while (menu == 1) ;

            if (menu > 4)
            {
                Console.WriteLine($"Please choose the correct option");
                Console.WriteLine($"Press <ENTER> to return to the main menu");
                Console.ReadLine();
                menu = 5;
                Console.Clear();

                if (menu == 4)
                {
                    var foods = new Dictionary<int, string>();
                    var count = 6;
                    var i = 1;


                    Console.WriteLine("Hi Please enter your 5 favorite foods");

                    while (i < count)
                    {
                        Console.WriteLine($"What is your #{i} Favourite Food? Please do not enter numbers");

                        var b = Console.ReadLine();


                        if (foods.ContainsValue(b))
                        {
                            Console.WriteLine("that food has already been entered");
                        }
                        else
                        {
                            foods.Add(i, b);
                            i++;


                        }
                    }
                    check(foods);
                    change(foods);
                }
            }
        }
        static void check(Dictionary<int, string> foods)
        {
            var a = foods.Count;

            if (a == 5)
            {
                foreach (var x in foods)
                    Console.WriteLine($"Your #{x.Key} favorite food is {x.Value}");
            }
            else
            {
                foods.Clear();
            }
        }
        static void change(Dictionary<int, string> foods)
        {
            Console.WriteLine("Do you wish to change any of your preferences? yes/no");
            var prefer = Console.ReadLine();

            if (prefer == "yes")
            {
            taco:
                Console.WriteLine("Please enter the number of the preference you wish to change");
                var value = int.Parse(Console.ReadLine());



                if ((value >= 1) && (value <= 5))
                {
                    switch (value)
                    {
                        case 1:
                            Console.WriteLine("Which food do you want to replace your #1 Choice with?");
                            foods[1] = Console.ReadLine();
                            break;
                        case 2:
                            Console.WriteLine("Which food do you want to replace your #2 Choice with?");
                            foods[2] = Console.ReadLine();
                            break;
                        case 3:
                            Console.WriteLine("Which food do you want to replace your #3 Choice with?");
                            foods[3] = Console.ReadLine();
                            break;
                        case 4:
                            Console.WriteLine("Which food do you want to replace your #4 Choice with?");
                            foods[4] = Console.ReadLine();
                            break;
                        case 5:
                            Console.WriteLine("Which food do you want to replace your #5 Choice with?");
                            foods[5] = Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("That is not a valid option");
                            break;
                    }

                }
                else
                {
                    Console.WriteLine("The number was bigger than 5");
                    goto taco;
                }

                for (var i = 1; i < foods.Count; i++)
                {
                    if (foods[value] == foods[i])
                    {
                        Console.WriteLine("that food has already been entered please select another one");
                        goto taco;
                    }
                    else
                    {
                        Console.WriteLine("you have no double ups");
                    }
                }
                check(foods);
                Console.ReadLine();
            }





            if (menu == 2)
                Console.WriteLine("Are you Level 5 or Level 6? Please enter '5' or '6'");
            var level = int.Parse(Console.ReadLine());


            {
                if (level == 5)
                {
                    var stuid = new List<string> { "10003633", "" };
                    var papers = new List<string> { "", "" };

                    Console.WriteLine("You are level 5");
                    Console.WriteLine("Please enter your Student ID number");
                    stuid.Add(Console.ReadLine());

                    Console.WriteLine("please enter paper 1");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res1 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 2");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res2 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 3");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res3 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 4");
                    papers.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res4 = int.Parse(Console.ReadLine());

                    int totalres = ((res1 + res2 + res3 + res4) / 4);
                    Console.WriteLine($"you have an overall mark of {totalres}%");


                    var overall = new List<int> { };
                    overall.Add(totalres);


                    Console.WriteLine("The overall results are:");
                    overall.ForEach(Console.WriteLine);
                    Console.WriteLine("Students that have entered their papers and results are:");
                    stuid.ForEach(Console.WriteLine);

                    Console.WriteLine("The Papers entered are:");
                    papers.ForEach(Console.WriteLine);

                    marks(totalres);
                    Console.ReadKey();
                }
                else if (level == 6)
                {
                    Console.WriteLine("You are level 6");
                    var stuid6 = new List<string> { "10003633", "" };
                    var papers6 = new List<string> { "", "" };

                    Console.WriteLine("Please enter your Student ID number");
                    stuid6.Add(Console.ReadLine());

                    Console.WriteLine("please enter paper 1");
                    papers6.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res16 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 2");
                    papers6.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res26 = int.Parse(Console.ReadLine());

                    Console.WriteLine("Please enter paper 3");
                    papers6.Add(Console.ReadLine());
                    Console.WriteLine("What % did you get for this paper?");
                    int res36 = int.Parse(Console.ReadLine());

                    int totalres6 = ((res16 + res26 + res36) / 3);
                    Console.WriteLine($"you have an overall mark of {totalres6}%");

                    var overall6 = new List<int> { };
                    overall6.Add(totalres6);
                    Console.WriteLine("The overall results are:");
                    overall6.ForEach(Console.WriteLine);
                    Console.WriteLine("Students that have entered their papers and results are:");
                    stuid6.ForEach(Console.WriteLine);
                    Console.WriteLine("The Papers entered are:");
                    papers6.ForEach(Console.WriteLine);
                    barks(totalres6);
                    Console.ReadKey();

                }
                else
                {
                    Console.WriteLine("Please enetr a valid input of either '5' or '6'");

                }
            }
        }



            static void marks(int totalres)
{


    if (totalres > 90)

    {
        Console.WriteLine("You have got an 'A+' overall Congratulations!");
    }

    else if (totalres > 85)
    {
        Console.WriteLine("You have got an 'A' overall Congratulations");
    }

    else if (totalres > 80)
    {
        Console.WriteLine("You have got an 'A-' overall");
    }

    else if (totalres > 75)
    {
        Console.WriteLine("You have got a 'B+' overall");
    }

    else if (totalres > 70)
    {
        Console.WriteLine("You have got a 'B' overall");
    }

    else if (totalres > 65)
    {
        Console.WriteLine("You have got a 'B-' overall");
    }

    else if (totalres > 60)
    {
        Console.WriteLine("You have got a 'C+' overall");
    }

    else if (totalres > 55)
    {
        Console.WriteLine("You have got a 'C' overall");
    }

    else if (totalres > 50)
    {
        Console.WriteLine("You have got a 'C-'' overall");
    }

    else if (totalres > 40)
    {
        Console.WriteLine("You have got a 'D' overall");
    }

    else if (totalres > 39)
    {
        Console.WriteLine("You have got an 'E' overall");
    }
}


static void barks(int totalres6)

{

    if (totalres6 > 90)

    {
        Console.WriteLine("You have got an 'A+' overall Congratulations!");
    }

    else if (totalres6 > 85)
    {
        Console.WriteLine("You have got an 'A' overall Congratulations");
    }

    else if (totalres6 > 80)
    {
        Console.WriteLine("You have got an 'A-' overall");
    }

    else if (totalres6 > 75)
    {
        Console.WriteLine("You have got a 'B+' overall");
    }

    else if (totalres6 > 70)
    {
        Console.WriteLine("You have got a 'B' overall");
    }

    else if (totalres6 > 65)
    {
        Console.WriteLine("You have got a 'B-' overall");
    }

    else if (totalres6 > 60)
    {
        Console.WriteLine("You have got a 'C+' overall");
    }

    else if (totalres6 > 55)
    {
        Console.WriteLine("You have got a 'C' overall");
    }

    else if (totalres6 > 50)
    {
        Console.WriteLine("You have got a 'C-' overall");
    }

    else if (totalres6 > 40)
    {
        Console.WriteLine("You have got a 'D' overall");
    }

    else if (totalres6 > 39)
    {
        Console.WriteLine("You have got an 'E' overall");
    }
}
    }


}





//Comment for testing